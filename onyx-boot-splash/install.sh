#!/usr/bin/env bash

COUNTER=1
EXT=png
PACKAGE="$(dirname "$0")"
THEME_PATH="$MNT/usr/share/plymouth/themes/onyx"

for IMG in "$PACKAGE"/*.$EXT ; do
	# printf -v NUM "%02d" $COUNTER
	if [ ! -f "$PACKAGE"/progress-$COUNTER.$EXT ]; then
		mv "$IMG" "$PACKAGE"/progress-$COUNTER.$EXT
	fi
	COUNTER=$(expr $COUNTER + 1)
done

COUNTER=1

for IMG in "$PACKAGE"/throbber/*.$EXT ; do
	printf -v NUM "%04d" $COUNTER
	if [ ! -f "$PACKAGE"/throbber/throbber-$NUM.$EXT ]; then
		mv "$IMG" "$PACKAGE"/throbber/throbber-$NUM.$EXT
	fi
	COUNTER=$(expr $COUNTER + 1)
done

# if [ -f "$PACKAGE"/../mnt/fusauto.conf ]; then
# 	"$PACKAGE"/../mount.sh
# fi

if [ ! -d "$THEME_PATH" ]; then
	mkdir "$THEME_PATH"
fi

cp "$PACKAGE"/*.png "$THEME_PATH"
cp "$PACKAGE"/throbber/*.png "$THEME_PATH"
	
