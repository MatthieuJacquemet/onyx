#!/usr/bin/env bash

COUNTER=1
EXT=png

for IMG in *.$EXT ; do
	printf -v NUM "%04d" $COUNTER
	mv "$IMG" throbber-$NUM.$EXT
	COUNTER=$(expr $COUNTER + 1)
done

if [ -f ../../mnt/fusauto.conf ]; then
	../../mount.sh
fi

cp *.png ../../mnt/usr/share/plymouth/themes/onyx
	
