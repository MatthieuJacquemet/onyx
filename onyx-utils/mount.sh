#/usr/bin/env bash

MNT="$(dirname "$0")/../mnt"

if [ ! -d "$MNT" ]; then
    mkdir "$MNT"
    #cp "$(dirname "$0")/utils/fusauto.conf" "$MNT"
fi

sshfs onyx:/ "$MNT"
