#include "log.h"
#include <chrono>
#include <ctime>
#include <cstring>
#include <iostream>

using namespace std;
using namespace onyx;

ofstream logger::_file(NULL_FILE);
bool logger::enable_timestamp(true);
InfoStream_t logger::info = InfoStream_t();
WarnStream_t logger::warn = WarnStream_t();
ErrorStream_t logger::error = ErrorStream_t();

void LogStream_t::write(const char*) {

    if (logger::enable_timestamp && logger::_file.is_open()){    
        auto clock = chrono::system_clock::now();
        time_t time = chrono::system_clock::to_time_t(clock);
        char* timestamp = ctime(&time);
        timestamp[strlen(timestamp)-2] = '\0'; //remove the \n at the end of time
        logger::_file << timestamp;
    }
}
ofstream& LogStream_t::operator <<(const char* message) {
    write(message);
    return logger::_file;
}

void InfoStream_t::write(const char* message) {
    if (logger::_file.is_open()){
        LogStream_t::write(nullptr);
        logger::_file << "  info : " << message;
    }
}

void WarnStream_t::write(const char* message) {
    if (logger::_file.is_open()){
        LogStream_t::write(nullptr);
        logger::_file << "  warning : " << message;
    }
}

void ErrorStream_t::write(const char* message) {
    if (logger::_file.is_open()){
        LogStream_t::write(nullptr);
        logger::_file << "  error : " << message;
    }
}

void logger::open(const char* name) {
    logger::_file.close();
    logger::_file.open(name);
}