#!python
import time
import os
from threading import RLock, Event
import pyinotify
from utils import main, Profiler, register, State, \
    thread, set_at, get_at, event, all_in, get_at
import inotify.adapters as inotify
from evdev import InputDevice, ecodes, list_devices

profiler = Profiler()

class GamepadError(Exception):
    pass

class ButtonError(GamepadError):
    pass

class AxisError(GamepadError):
    pass

class StateError(GamepadError):
    pass

class CodeError(GamepadError):
    pass


class Gamepad:

    def __init__(self, device=None, stay_alive=False, start=True,
        ignored_events=(ecodes.EV_SYN,ecodes.EV_MSC)):
        
        self.handlers = self.__class__.handlers
        self.ignored_events = ignored_events
        self.access_lock    = RLock()
        self.key_callbacks  = list()
        self.abs_callbacks  = list()
        self.any_event      = list()
        self.axis           = list()
        self.device         = device
        self.stop_event     = Event()
        self.stay_alive     = stay_alive

        if not self.device:
            self.find_new_device()
        if self.stay_alive():
            self.notifier = inotify.Inotify("/dev/input", block_duration_s=0)


    def __new__(cls, *args, **kwargs):
        cls.handlers = list()
        for method in vars(cls).values():
            id = getattr(method, "_id", None)
            if id:
                set_at(cls.handlers, method, id, None)
        return super().__new__(cls)

    def plug_event(self, event):
        # path = event.pathname
        # if os.path.basename(path).startswith("event"):
        for path in list_devices():
            dev = InputDevice(path)
            if is_gamepad(dev):
                self.device = dev
                self.event_loop()
                break

    def find_new_device(self):
        for path in list_devices():
            dev = InputDevice(path)
            if is_gamepad(dev):
                self.device = dev
                break

    @thread
    def event_loop(self):
        loop = True
        while loop:
            if not self.find_new_device():
            header, *_ = next(notifier.event_gen()):
            if not header == None:
                if header.mask & event.IN_CRE
            try:
                for event in self.device.read_loop():
                    if self.stop_event.is_set():
                        break
                    if not event.type in self.ignored_events:
                        self.dispatch(event)
                
                        for callback in self.any_event:
                            callback(event.type, event.code, event.value)
            except Exception as e:
                print(e)
            loop = self.stay_alive
        
    def dispatch(self, event):
        handle = get_at(self.handlers, event.type)
        if handle:
            return handle(self, event.code, event.value, event.timestamp)
        return False

    @register(ecodes.EV_ABS)
    def analog(self, code, state, timestamp):
        value = state/255 - 0.5
        set_at(self.axis, value, code, 0)
        cb = get_at(self.abs_callbacks, code)
        if cb == None:
            return
        for callback in cb:
            callback(value, code)

    @register(ecodes.EV_KEY)
    def key(self, code, state, timestamp):
        
        cb = get_at(self.key_callbacks, code)
        if cb == None:
            return
        for callback in cb[state]:
            callback(state, code)

    def get_axis(self, id):
        value = get_at(self.axis, id)
        if value == None:
            set_at(self.axis, 0, id, 0)
            return 0
        return value
    
    def register_callback(self, callback, code, state):
        cb = get_at(self.key_callbacks, code)
        if cb == None:
            set_at(self.key_callbacks, ([], [], []), code, None)
        try:
            self.key_callbacks[code][state].append(callback)
        except IndexError:
            raise StateError("invalid state value")

    def unregister_callback(self, callback, code, state):
        try:
            self.key_callbacks[code][state].remove(callback)
        except (IndexError, KeyError, ValueError):
            print(f"cannot unregister {callback} {code} {state}")

    def register_axis(self, callback, code):
        cb = get_at(self.abs_callbacks, code)
        if cb == None:
            set_at(self.abs_callbacks, list(), code, None)
        try:
            self.abs_callbacks[code].append(callback)
        except IndexError:
            raise StateError("invalid state value")

    def unregister_axis(self, callback, code):
        try:
            self.abs_callbacks[code].remove(callback)
        except (IndexError, KeyError, ValueError):
            print(f"cannot unregister {callback} {code}")

    def register_any(self, callback):
        self.any_event.append(callback)

    def unregister_any(self, callback):
        self.any_event.remove(callback)

if __name__ == "__main__":
    _default=Gamepad(stay_alive=True)

def is_gamepad(device):
    reqs = (ecodes.BTN_SOUTH,ecodes.BTN_NORTH,
            ecodes.BTN_WEST,ecodes.BTN_TL,
            ecodes.BTN_TR,ecodes.BTN_TL2,
            ecodes.BTN_TR2,ecodes.BTN_SELECT,
            ecodes.BTN_START,ecodes.BTN_MODE,
            ecodes.BTN_DPAD_UP,ecodes.BTN_DPAD_DOWN,
            ecodes.BTN_DPAD_LEFT,ecodes.BTN_DPAD_RIGHT)

    caps = device.capabilities()
    keys = caps.get(ecodes.EV_KEY, list())
    return all_in(reqs, keys)

def enum_gamepad(id):
    for path in list_devices():
        device = InputDevice(path)
        if is_gamepad(device):
            yield Gamepad(device, start=False)
            
def get_default_gamepad():
    return _default
    
# manager = GamepadManager("Sony PLAYSTATION(R)3 Controller")