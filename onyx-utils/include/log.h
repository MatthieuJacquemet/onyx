#pragma once
#include <fstream>


namespace onyx {

    #define NULL_FILE "/dev/null"
    
    class LogStream_t {
        public:
        virtual void write(const char* message);
        std::ofstream& operator <<(const char* message);
    };

    class DummyStream {
        template<typename T>
        DummyStream& operator <<(T message);
    };

    class InfoStream_t: public LogStream_t {
        public:
        void write(const char* message);
    };

    class WarnStream_t: public LogStream_t {
        public:
        void write(const char* message);
    };

    class ErrorStream_t: public LogStream_t {
        public:
        void write(const char* message);
    };

    class logger {

        public:
        static std::ofstream _file;
        static bool enable_timestamp;
        static void open(const char* name);
        static InfoStream_t info;
        static WarnStream_t warn;
        static ErrorStream_t error;
    };

}