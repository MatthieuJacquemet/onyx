#if defined _WIN32 || defined __CYGWIN__
    #include "onyx"


#elif defined(__linux__) || defined(__APPLE__)

#endif

#ifdef _WIN32

#elif defined(__linux__) || defined(__APPLE__)

#endif

  #ifdef WIN_EXPORT
    #ifdef __GNUC__
      #define EXPORT __attribute__ ((dllexport))
    #else
      #define EXPORT __declspec(dllexport)
    #endif
  #else
    #ifdef __GNUC__
      #define EXPORT __attribute__ ((dllimport))
    #else
      #define EXPORT __declspec(dllimport)
    #endif
  #endif
  #define NO_EXPORT
#else
  #if __GNUC__ >= 4
    #define EXPORT __attribute__ ((visibility ("default")))
    #define NO_EXPORT  __attribute__ ((visibility ("hidden")))
  #else
    #define EXPORT
    #define NO_EXPORT
  #endif
#endif