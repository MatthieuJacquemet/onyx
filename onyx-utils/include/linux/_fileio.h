    #include <sys/stat.h> 
    #include <sys/stat.h>
    #include <fcntl.h>
    #include <unistd.h>
    
    #define _open(path, option) open(path, option)
    #define _read(fd, buf, size) read(fd, buf, size)
    #define _write(fd, buf, size) write(fd, buf, size)
    #define _close(fd) close(fd)
    #define _seek(fd, dist, base) lseek(fd, dist, base)
    #define READ O_RDONLY
    #define WRITE O_WRONLY
    #define BEGIN SEEK_SET
    #define CURRENT SEEK_CUR
    #define END SEEK_END
    #define WINVARS(vars)