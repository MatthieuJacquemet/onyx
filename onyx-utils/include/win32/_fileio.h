    #include "Windows.h"
    #define _open(path, option) OpenFile(path, &fstc, option)
    #define _read(fd, buf, size) ReadFile((HANDLE)fd, buf, size, &bytesread, NULL)
    #define _write(fd, buf, size) WriteFile((HANDLE)fd, buf, size, &byteswrite, NULL)
    #define _close(fd) CloseHandle((HANDLE)fd)
    #define _seek(fd, dist, base) SetFilePointer((HANDLE)fd, dist, NULL, base)
    #define READ OF_READ
    #define WRITE OF_WRITE
    #define READ_WRITE OF_READWRITE
    #define BEGIN FILE_BEGIN
    #define CURRENT FILE_CURRENT
    #define END FILE_END
    #define WINVARS(vars) vars