#pragma once

#include <dlfcn.h>
#include <vector>
#include <string.h>
#include "log.h"

#define _API_CALL_WARN "calling api but not initialized"
#define _API_LOAD_ERR "error while loading library : "
#define _API_SYM_ERR "cannot find symbol for : "
#define _API_CONST_BASE 101

#ifndef SHARED

typedef std::pair<const char*, void**> _handle_t;
static std::vector<_handle_t> _api_handles
 __attribute__((init_priority(_API_CONST_BASE)));

void* load_library(const char* name, int mode=RTLD_LAZY) {
    void* lib_ptr = dlopen(name, mode);
    if (lib_ptr) {
        for (auto& it: _api_handles) {
            void* sym = dlsym(lib_ptr, it.first);
            if (sym)
                *(it.second) = sym;
#ifdef DEBUG
            else onyx::logger::error << _API_SYM_ERR << 
                it.first << ", " << strerror(errno) << std::endl;
        }
    } else onyx::logger::error << _API_LOAD_ERR <<
        strerror(errno) << std::endl;
#else
    }}
#endif
    return lib_ptr;
}

template<class ret_t, class ...Args>
ret_t _api_trap(Args... args) {
#ifdef DEBUG
    onyx::logger::warn << _API_CALL_WARN << std::endl;
#endif
    return ret_t();
}

#define REGISTER_API_FCN(ret, name, ...) \
static ret (*name)(__VA_ARGS__) = &_api_trap<ret, __VA_ARGS__>; \
__attribute__((constructor(_API_CONST_BASE + 1 + __COUNTER__))) \
static void _API_LOAD_##name () { \
    /*register the function in the handles*/ \
    _api_handles.push_back(_handle_t(#name, (void**)&name));}

#else
#define REGISTER_API_FCN(ret, name, ...)
#endif