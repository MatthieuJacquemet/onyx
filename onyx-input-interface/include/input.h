#include "frame.h"

namespace onyx_input {

    void push_frame(Frame_t* frame);
    void pop_frame(Frame_t* frame);
}