cmake_minimum_required(VERSION 3.0)
project (onyx-aii)

set(EXECUTABLE_OUTPUT_PATH "out")
set(SRC_DIR src)
# set(HDR_DIR include)

set(SRCS
    ${SRC_DIR}/main.cpp
)

link_directories(../onyx-utils/out)
add_library(${PROJECT_NAME} ${SRCS})
target_link_libraries(${PROJECT_NAME} onyx-utils)
target_include_directories(${PROJECT_NAME} PUBLIC ../onyx-utils/include)

target_compile_features(${PROJECT_NAME} PUBLIC cxx_nullptr)
target_include_directories(${PROJECT_NAME} PUBLIC include ../onyx-utils/include)
