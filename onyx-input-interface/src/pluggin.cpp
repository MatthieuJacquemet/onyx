#include <vector>
#include <string>
#include <filesystem>
#include <dlfcn.h>

#define PLUGIN_DIR "/lib/onyx_input_pluggin"
#define PLUGIN_NAME "libinput-pluggin.so"

using namespace std;

vector<void*> load_pluggins() {

    vector<void*> pluggins;
    for (const auto & entity : filesystem::directory_iterator(PLUGIN_DIR)) {

        if (entity.is_directory()) {

            filesystem::path path = entity.path / PLUGIN_NAME;
            if (filesystem::exists(path)) {
                if (void* lib = dlopen(path.c_str(), RTLD_LAZY))
                    pluggins.push_back(lib);
            }
        
        } else if (entity.is_regular_file()) {

            if (void* lib = dlopen(entity.path.c_str(), RTLD_LAZY))
                pluggins.push_back(lib);
        } 
    }
}