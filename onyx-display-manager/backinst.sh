#!/usr/bin/env bash
PACKAGE="$(dirname "$0")"
cp "$PACKAGE/onyx-dm" "$MNT/sbin"
cp "$PACKAGE/onyx-dm.service" "$MNT/lib/systemd/system"
cp -r "$PACKAGE/display-manager" "$MNT/usr/local/lib/python3.7"
