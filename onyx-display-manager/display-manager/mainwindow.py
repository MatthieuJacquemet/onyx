import sys
import os

from qtpy.QtWidgets import QMainWindow
from utils import main, parse_args

@main(**parse_args(sys.argv))
class DisplayManager(QMainWindow):

    def __init__(self, exe=None):
        super().__init__([])

        common.gamepad = GamepadManager(properties["controller"])

        self.using_mouse=True
        self.installEventFilter(self)
        self.gamepad_signal.connect(self.hide_mouse)
        common.gamepad.register_any(self.gamepad_event)

        self.data_path = join_module_path("data.xml")

        self.apply_style(":/style/MainWindow.css")

        self.main_window = MainWindow()
        ex_code = self.exec()
        common.stop_event.set()
        sys.exit(ex_code)
