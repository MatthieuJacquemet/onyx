#include <dlfcn.h>
#include <iostream>
#include <string.h>
#include "api.h"
#include "utils.h"
#include <gtkmm.h>

#define PROVIDER_PATH "./content-providers/"

using namespace std;
using namespace onyx;

int main(int argc, char *argv[]) {

    load_library( PROVIDER_PATH "ygg-provider/libygg-provider.so");
    api::content_list_t r = api::get_last(api::content_type_t::MOVIE, 1);
    cout << r.front().name << endl;

    auto app = Gtk::Application::create(argc, argv, "org.gtkmm.examples.base");

    Gtk::Window window;
    window.set_default_size(500, 400);

    return app->run(window);
}