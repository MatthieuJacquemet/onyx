#include <iostream>
#include "api.h"

using namespace std;

extern "C" {    
    api::content_list_t get_last(api::content_type_t type, unsigned short max_items=32){
        api::content_list_t res;
        api::Content_t* cc = new api::Content_t();
        cc->name = "hello world";
        res.push_back(*cc);
        return res;
    }
    api::content_list_t get_trend(api::content_type_t type, unsigned short max_item=32){
        cout << "this is tend" << endl;
        api::content_list_t res;
        api::Content_t* cc = new api::Content_t();
        cc->name = "trend";
        res.push_back(*cc);
        return res;
    }
    api::content_list_t find(const char* name){
        cout << "this is find" << endl;
        api::content_list_t res;
        api::Content_t* cc = new api::Content_t();
        cc->name = "find";
        res.push_back(*cc);
        return res;
    }
}
