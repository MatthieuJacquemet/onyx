#include <iostream>
#include <curl/curl.h>

#define YGG_TWITTER "https://twitter.com/yggtorrent_com"

using namespace std;

char* get_ygg_domain()
{
  CURL *curl;
  CURLcode res;

  curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, YGG_TWITTER);
    res = curl_easy_perform(curl);

    /* always cleanup */ 
    curl_easy_cleanup(curl);
  }
  return 0;