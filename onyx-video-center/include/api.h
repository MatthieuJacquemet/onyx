#include <list>
#include "utils.h"

namespace api {

    enum content_type_t {
        MOVIE=0x0,
        SERIE,
        DOCUMENTARY,
        CONCERT,
        SPECTACLE,
        GAME,
    };

    struct Content_t {
        const char* name;
        const char* author;
        const char* access_path;
        const char* cover_path;
    };

    typedef std::list<Content_t> content_list_t;

    //#include "gen.h"
    REGISTER_API_FCN(content_list_t, get_last, content_type_t, unsigned short)
    REGISTER_API_FCN(content_list_t, get_trend, content_type_t, unsigned short)
    REGISTER_API_FCN(void, test, content_type_t)

}